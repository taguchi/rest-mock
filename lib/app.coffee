#
# Module dependencies.
#
_         = require 'lodash'
Promise   = require 'bluebird'
path      = require 'path'
Datastore = require 'nedb'
express   = require 'express'

db = {}

#
# Express middleware
#  - Cross-Origin Resource Sharing 
#  - Parse JSON
#  - Connect NeDB
#  
app = express()

app.use (req, res, next) ->
  res.setHeader "Access-Control-Allow-Origin", req.headers.origin || "*"
  res.setHeader "Access-Control-Allow-Credentials", true
  res.setHeader "Access-Control-Allow-Methods",
    "POST, PUT, PATCH, GET, DELETE, OPTIONS"
  res.setHeader "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Origin, Content-Type, Accept, Authorization"
  next()

app.use require("body-parser").json()
app.use (err, req, res, next) ->
  if err instanceof SyntaxError and err.body
    return res.status(400).send message: "invalid json parameters"
  next()

app.param "collection", (req, res, next, collection) ->
  unless db[collection]
    db[collection] = Promise.promisifyAll(new Datastore(path.resolve(__dirname + "/../db/#{collection}.db")))
    db[collection].loadDatabase()
  @collection = db[collection]
  next()

#
# Root
#
if process.env.STATIC_DIR
  app.use('/', express.static(process.env.STATIC_DIR));
else
  app.get '/', (req, res, next) ->
    res.status(200).send(message: "Hello RestMock")

#
# Create Record
#
app.post "/:collection", (req, res, next) ->
  @collection.insertAsync(req.body)
    .then((doc) -> res.status(201).json(doc) )
    .catch(next)

#
# Retrieve Record
#
app.get "/:collection/:id", (req, res, next) ->
  @collection.findOneAsync(_id:req.params.id)
    .then((doc) ->
      if (doc)
        res.status(200).json(doc)
      else
        res.status(404).json(message: "resource not found")
    )
    .catch(next)

#
# Update Record
#
app.patch "/:collection/:id", (req, res, next) ->

  @collection.findOneAsync(_id:req.params.id)
  .then((doc) =>
    if doc
      updated = _.assign(doc, req.body)
      @collection.updateAsync(_id:req.params.id, updated, {})
      .then (num) ->
        res.status(200).json(updated)
    else
      res.status(404).json(message:"resource not found")
   )
  .catch(next)

#
# Remove Record
#
app.delete "/:collection/:id", (req, res, next) ->
  @collection.removeAsync(_id:req.params.id)
    .then((num) ->
      if num is 1
        res.status(204).send()
      else
        res.status(404).json(message:"resource not found")
    ).catch(next)
#
# Find Record
#
app.get "/:collection", (req, res, next) ->
  {limit, page, query, sort} = (({limit, page, query, sort}) ->
    limit = Number(limit) or 10000
    page  = Number(page) or 1
    query = query || {}
    sort  = _.chain((sort || '').split(','))
      .map((key) -> ["#{key.replace(/^-/, '')}",(if key.match(/^-/) then -1 else 1 )] )
      .zipObject().value()
    try
      query = JSON.parse query
    catch err
    query = req.body if req.body and !query
    return {query, limit, page, sort}
  )(req.query)

  @collection.countAsync(query)
    .then((num) ->
      new Promise((resolve, reject) =>
        p = @collection.find(query)
        p = p.sort(sort)           if sort
        p = p.skip((page-1)*limit) if page > 1
        p = p.limit(limit)         if limit
        p.exec((err, docs) -> if err then reject(err) else resolve(docs))
      )
    )
    .then((docs) -> res.status(200).json rows:docs )
    .catch(next)

#
# Express middleware
# - Error
#
app.use (err, req, res, next) -> res.status(500).send message: err.message

module.exports = app