request = require 'supertest'
assert  = require 'power-assert'
app     = require '../../lib/app'

describe "curd mock", ->
  uri = "/testdb"
  hoge =
    title: "hoge"
    description: "hoge description"
    count: 0
    created: new Date()

  describe "create recoed", ->
    it "create api should be success", (done) ->
      request(app)
        .post "#{uri}"
        .send hoge
        .expect "Content-Type", /json/
        .expect 201
        .end (err, res) ->
          assert(err is null)
          assert(res.body.title is hoge.title)
          assert(res.body.description is hoge.description)
          assert(res.body.created is hoge.created.toISOString())
          hoge._id = res.body._id
          done()

  describe "retrieve record", ->
    it "get api should be success", (done) ->
      request(app)
        .get "#{uri}/#{hoge._id}"
        .expect 200
        .expect "Content-Type", /json/
        .end (err, res) ->
          assert(err is null)
          assert(res.body._id is hoge._id)
          assert(res.body.title is hoge.title)
          assert(res.body.description is hoge.description)
          assert(res.body.created is hoge.created.toISOString())
          done()

    it "should be returned 404 if not exist record", (done) ->
      request(app)
        .get "#{uri}/invalid-id"
        .expect 404
        .expect "Content-Type", /json/
        .end (err, res) ->
          assert(err is null)
          assert(res.body.message is "resource not found")
          done()

  describe "update record", ->

    it "update api should be success", (done) ->
      request(app)
        .patch "#{uri}/#{hoge._id}"
        .send
          title: "update"
        .expect 200
        .expect "Content-Type", /json/
        .end (err, res) ->
          assert(err is null)
          assert(err is null)
          assert(res.body._id is hoge._id)
          assert(res.body.title is "update")
          assert(res.body.description is hoge.description)
          assert(res.body.created is hoge.created.toISOString()) 
          done()

    it "should be returned 404 if not exist record", (done) ->
      request(app)
        .patch "#{uri}/invalid-id"
        .send
          description: null
        .expect 404
        .expect "Content-Type", /json/
        .end (err, res) ->
          assert(err is null)
          assert(res.body.message is "resource not found")
          done()

  describe "delete record", ->
    it "delete api should be success", (done) ->
      request(app)
        .delete "#{uri}/#{hoge._id}"
        .expect 204
        .end (err, res) ->
          assert(err is null)
          done()

    it "should be returned 404 if not exist record", (done) ->
      request(app)
        .delete "#{uri}/#{hoge._id}"
        .expect 404
        .expect "Content-Type", /json/
        .end (err, res) ->
          assert(err is null)
          assert(res.body.message is "resource not found")
          done()

