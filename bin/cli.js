#!/usr/bin/env node
require('coffee-script/register');

var path = require("path");
var app = require(path.resolve(__dirname + "/../lib/app"));

app.listen(process.env.PORT||8080)

console.log("Start server http://localhost:" + (process.env.PORT||8080));

process.on("uncaughtException", function(err) {
  console.log(err.message, err.stack);
  return process.exit();
});
