# RESTful Mock API

Mock Server (express + NeDB)


## Start Server
```
npm install -g "git://github.com/taguch1/rest-mock.git
PORT=8080 rest-mock

curl http://localhost:8080
```


## Create new Record
---
Request
```
curl -v -X POST -H "Content-Type: application/json" \
 -d '{
  "title": "sample title",
  "description": "sample description"
}' \
 "http://localhost:8080/${collection}" | jq .
```

Response
```
content-type: application/json; charset=utf-8
status: 200 OK

{
  "title": "sample title",
  "description": "sample description",
  "_id": "LMLiHXP0Gp2szBQk"
}
```

### List Records
---
Request
```
curl -v -X GET -H "Content-Type: application/json" \
  -d '{"title":"sample title"}' \
  "http://localhost:8080/${collection}?limit=2&page=1&sort=-_id" | jq .

or

curl -v -X GET -H "Content-Type: application/json" \
  "http://localhost:8080/${collection}?limit=2&page=1&sort=-_id&query=%7B%22title%22%3A%22${collection}+title%22%7D" | jq .
```

Response
```
content-type: application/json; charset=utf-8
status: 200 OK

[
  {
    /* JSON */
  },
]
```

* query - like [Mongo Query Documents](http://docs.mongodb.org/manual/tutorial/query-documents/)
* limit - レコードリミット数
* page  - レコードのページ
* sort  - ソートカラムをカンマ区切りで指定,降順は先頭にマイナスを指定する


### Retrieve an existing Record
---
Request
```
curl -s -X GET -H "Content-Type: application/json" \
  "http://localhost:8080/${collection}/${id}" | jq .
```

Response
```
content-type: application/json; charset=utf-8
status: 200 OK

{
  /* JSON */
}
```


### Update a Record
---
Request
```
curl -s -X PATCH \
  -H "Content-type: application/json" \
  -d '{
    "title": "update title"
  }' \
  "http://localhost:8080/${collection}/${id}" | jq .
```

Response
```
content-type: application/json; charset=utf-8
status: 200 OK

{
  /* JSON */
}
```


### Delete a Record
---
Request
```
curl -s -X DELETE -H "Content-Type: application/json" \
  "http://localhost:8080/${collection}/${id}"
```

Response
```
status: 204 No Content
```